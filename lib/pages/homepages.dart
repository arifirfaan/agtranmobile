import 'package:agtranandroid/pages/hotdeal.dart';
import 'package:agtranandroid/pages/topcar.dart';
import 'package:agtranandroid/pages/voucher.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class Homepages extends StatefulWidget {
  Homepages({Key key}) : super(key: key);

  @override
  _HomepagesState createState() => _HomepagesState();
}

class _HomepagesState extends State<Homepages> {


  @override
  Widget build(BuildContext context) {
    return Container(
       child: ListView(children: <Widget>[
         Container(
              height: 60,
              padding: EdgeInsets.all(10),
              color: Colors.red,
              child: RaisedButton(
                color: Colors.white,
                shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                onPressed: (){},
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                  Text('Search', style: TextStyle(fontSize: 15),),
                  IconButton(icon: Icon(Icons.search), onPressed: (){})
                ],
                ),
              ),
            ),
            Container(
              child: CurrentBooking(), //class ada di bawah sekali
            ),
            Align(
              alignment: Alignment.center,
              child: Container( 
              child: DetailOrder(),
            ),
            ),
            Container(
                margin: EdgeInsets.symmetric(horizontal:20, vertical:10),
                child: Column(
                children: <Widget>[
                Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                Flexible(
                  child: Text('Hot deal!', style: TextStyle(fontWeight: FontWeight.w600, 
                  fontSize: 15,
                  shadows:<Shadow>[
                  Shadow( offset: Offset(1.0,2.0),
                  blurRadius: 3.0,
                  color: Colors.grey
                  )
                  ]
                  ),),
                ),
                FlatButton(
                  onPressed: (){
                    Navigator.push(
                      context, MaterialPageRoute(
                        builder: (_) => HotDeal()
                      )
                    );
                  },
                  child: Text('view all', style: TextStyle(color: Colors.red, fontWeight: FontWeight.w600,
                  shadows:<Shadow>[
                  Shadow( offset: Offset(1.0,2.0),
                  blurRadius: 3.0,
                  color: Colors.grey
                  )
                  ]
                  ),
                  ),
                  )
                  ],
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text('Book your car early and get up to discount', style: TextStyle(fontWeight: FontWeight.w600, 
                    fontSize: 12,
                    shadows:<Shadow>[
                    Shadow( offset: Offset(1.0,2.0),
                    blurRadius: 3.0,
                    color: Colors.grey
                    )
                    ]
                    )
                    ),
                ),
                Container(
                height: 120.0,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    GestureDetector(
                      child: Container(
                      margin: EdgeInsets.all(8), 
                      width:140,
                      height: 40,
                    decoration: BoxDecoration(
                    image: DecorationImage(
                      image:AssetImage("assets/images/myvimerah.jpg"), 
                      fit:BoxFit.fill,
                    ),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.8),
                        spreadRadius: 4,
                        blurRadius: 3,
                        offset: Offset(2, 1), // changes position of shadow
                      ),
                    ],
                  )
                  ),onTap:(){
                  print("you clicked my");
                  }
                ),
                Padding(padding: EdgeInsets.all(5.00)),
                GestureDetector(
                  child: Container(
                  margin: EdgeInsets.all(8), 
                  width:140,
                  height: 40,
                  child: Text(""),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image:AssetImage("assets/images/axiasilver.jpg"), 
                      fit:BoxFit.fill,
                    ),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.8),
                        spreadRadius: 4,
                        blurRadius: 3,
                        offset: Offset(2, 1), // changes position of shadow
                      ),
                    ],
                  )
                  ),onTap:(){
                  print("you clicked my");
                  }
                ),
                Padding(padding: EdgeInsets.all(5.00)),
                 GestureDetector(
                  child: Container(
                  margin: EdgeInsets.all(8), 
                  width:140,
                  height: 40,
                  child: Text(""),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image:AssetImage("assets/images/myvimerah.jpg"), 
                      fit:BoxFit.fill,
                    ),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.8),
                        spreadRadius: 4,
                        blurRadius: 3,
                        offset: Offset(2, 1), // changes position of shadow
                      ),
                    ],
                  )
                  ),onTap:(){
                  print("you clicked my");
                  }
                ),
                Padding(padding: EdgeInsets.all(5.00)),
                 GestureDetector(
                  child: Container(
                  margin: EdgeInsets.all(8), 
                  width:140,
                  height: 40,
                  child: Text(""),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image:AssetImage("assets/images/myvimerah.jpg"), 
                      fit:BoxFit.fill,
                    ),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.8),
                        spreadRadius: 4,
                        blurRadius: 3,
                        offset: Offset(2, 1), // changes position of shadow
                      ),
                    ],
                  )
                  ),onTap:(){
                  print("you clicked my");
                  }
                ),
                ],
                ),
              ),
              Container(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                Flexible(
                  child: Text('Voucher!', style: TextStyle(fontWeight: FontWeight.w600, 
                  fontSize: 15,
                  shadows:<Shadow>[
                  Shadow( offset: Offset(1.0,2.0),
                  blurRadius: 3.0,
                  color: Colors.grey
                  )
                  ]
                  ),
                  ),
                ),
                FlatButton(
                  onPressed: (){
                    Navigator.push(
                      context, MaterialPageRoute(
                        builder: (_) => Voucher()
                      )
                    );
                  },
                  child: Text('view all', style: TextStyle(color: Colors.red, fontWeight: FontWeight.w600,
                  shadows:<Shadow>[
                  Shadow( offset: Offset(1.0,2.0),
                  blurRadius: 3.0,
                  color: Colors.grey
                  )
                  ]
                  ),
                  ),
                  )
                  ],
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text('Rent a car and stand a change to get the voucher', style: TextStyle(fontWeight: FontWeight.w600, 
                    fontSize: 12,
                    shadows:<Shadow>[
                    Shadow( offset: Offset(2.0,2.0),
                    blurRadius: 3.0,
                    color: Colors.grey
                    )
                    ]
                    )
                    ),
                ),
                Container(
                  height: 120,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                    GestureDetector(
                    child: Container(
                    margin: EdgeInsets.all(8), 
                    width:200,
                    height: 40,
                    child: Text(""),
                    decoration: BoxDecoration(
                    image: DecorationImage(
                      image:AssetImage("assets/images/voucher1.png"), 
                      fit:BoxFit.fill,
                    ),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.8),
                        spreadRadius: 4,
                        blurRadius: 3,
                        offset: Offset(2, 1), // changes position of shadow
                      ),
                    ],
                    )
                    ),onTap:(){
                    print("you clicked my");
                    }
                  ),
                  GestureDetector(
                    child: Container(
                    margin: EdgeInsets.all(8), 
                    width:200,
                    height: 40,
                    child: Text(""),
                    decoration: BoxDecoration(
                    image: DecorationImage(
                      image:AssetImage("assets/images/voucher2.png"), 
                      fit:BoxFit.fill,
                    ),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.8),
                        spreadRadius: 4,
                        blurRadius: 3,
                        offset: Offset(2, 1), // changes position of shadow
                      ),
                    ],
                    )
                    ),onTap:(){
                    print("you clicked my");
                    }
                  )
                  ],
                  ),
                ),
                Container(
                  height: 20,
                ),
                Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                Flexible(
                  child: Text('Top Car!', style: TextStyle(fontWeight: FontWeight.w600, 
                  fontSize: 15,
                  shadows:<Shadow>[
                  Shadow( offset: Offset(1.0,2.0),
                  blurRadius: 3.0,
                  color: Colors.grey
                  )
                  ]
                  ),
                  ),
                ),
                FlatButton(
                  onPressed: (){
                    Navigator.push(
                      context, MaterialPageRoute(
                        builder: (_) => Tab1()
                      )
                    );
                  },
                  child: Text('view all', style: TextStyle(color: Colors.red, fontWeight: FontWeight.w600,
                  shadows:<Shadow>[
                  Shadow( offset: Offset(1.0,2.0),
                  blurRadius: 3.0,
                  color: Colors.grey
                  )
                  ]
                  ),
                  ),
                  )
                  ],
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text('Drive premium and pay economy', style: TextStyle(fontWeight: FontWeight.w600, 
                    fontSize: 12,
                    shadows:<Shadow>[
                    Shadow( offset: Offset(2.0,2.0),
                    blurRadius: 3.0,
                    color: Colors.grey
                    )
                    ]
                    )
                    ),
                ),
                Container(
                  height: 165,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                    GestureDetector(
                    child: Container(
                    margin: EdgeInsets.all(8), 
                    width:100,
                    height: 40,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.8),
                          spreadRadius: 4,
                          blurRadius: 3,
                          offset: Offset(2, 1), // changes position of shadow
                        ),
                        ],
                    ),
                    child: Column(children: <Widget>[
                      Container(
                        height: 100,
                        decoration: BoxDecoration(
                        image: DecorationImage(
                        image:AssetImage("assets/images/myvirating.png"), 
                        fit:BoxFit.fill,
                        ),
                        borderRadius:  BorderRadius.only(topRight:  Radius.circular(5),topLeft: Radius.circular(5)), 
                      )
                      ),
                      Container(
                        height: 15,
                        decoration: BoxDecoration(
                        color: Colors.red,
                        image: DecorationImage(
                        image:AssetImage("assets/images/2.5star.png"), 
                        fit:BoxFit.fitHeight,
                        ),
                      )
                      ),
                      Container(
                        width: 1000,
                        padding: EdgeInsets.all(2),
                        color: Colors.red,
                        child: Text('Perodua Myvi', style: TextStyle(fontSize: 10, color: Colors.white, fontWeight: FontWeight.w600),),
                      ),
                      Container(
                        width: 1000,
                        padding: EdgeInsets.all(2),
                        
                        child: Text('100 Rent', style: TextStyle(fontSize: 10, color: Colors.white, fontStyle: FontStyle.italic),),
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius:  BorderRadius.only(bottomRight:  Radius.circular(5),bottomLeft: Radius.circular(5)),
                        ),
                      ),

                    ],
                    ),
                   
                    ),onTap:(){
                    print("you clicked my");
                    }
                  ),
                  GestureDetector(
                    child: Container(
                    margin: EdgeInsets.all(8), 
                    width:100,
                    height: 40,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.8),
                          spreadRadius: 4,
                          blurRadius: 3,
                          offset: Offset(2, 1), // changes position of shadow
                        ),
                        ],
                    ),
                    child: Column(children: <Widget>[
                      Container(
                        height: 100,
                        decoration: BoxDecoration(
                        image: DecorationImage(
                        image:AssetImage("assets/images/bezzarating.png"), 
                        fit:BoxFit.fill,
                        ),
                        borderRadius:  BorderRadius.only(topRight:  Radius.circular(5),topLeft: Radius.circular(5)), 
                      )
                      ),
                      Container(
                        height: 15,
                        decoration: BoxDecoration(
                        color: Colors.red,
                        image: DecorationImage(
                        image:AssetImage("assets/images/3.5star.png"), 
                        fit:BoxFit.fitHeight,
                        ),
                      )
                      ),
                      Container(
                        width: 1000,
                        padding: EdgeInsets.all(2),
                        color: Colors.red,
                        child: Text('Perodua Bezza', style: TextStyle(fontSize: 10, color: Colors.white, fontWeight: FontWeight.w600),),
                      ),
                      Container(
                        width: 1000,
                        padding: EdgeInsets.all(2),
                        
                        child: Text('100 Rent', style: TextStyle(fontSize: 10, color: Colors.white, fontStyle: FontStyle.italic),),
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius:  BorderRadius.only(bottomRight:  Radius.circular(5),bottomLeft: Radius.circular(5)),
                        ),
                      ),

                    ],
                    ),
                   
                    ),onTap:(){
                    print("you clicked my");
                    }
                  ),
                  GestureDetector(
                    child: Container(
                    margin: EdgeInsets.all(8), 
                    width:100,
                    height: 40,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.8),
                          spreadRadius: 4,
                          blurRadius: 3,
                          offset: Offset(2, 1), // changes position of shadow
                        ),
                        ],
                    ),
                    child: Column(children: <Widget>[
                      Container(
                        height: 100,
                        decoration: BoxDecoration(
                        image: DecorationImage(
                        image:AssetImage("assets/images/myvirating.png"), 
                        fit:BoxFit.fill,
                        ),
                        borderRadius:  BorderRadius.only(topRight:  Radius.circular(5),topLeft: Radius.circular(5)), 
                      )
                      ),
                      Container(
                        height: 15,
                        decoration: BoxDecoration(
                        color: Colors.red,
                        image: DecorationImage(
                        image:AssetImage("assets/images/4star.png"), 
                        fit:BoxFit.fitHeight,
                        ),
                      )
                      ),
                      Container(
                        width: 1000,
                        padding: EdgeInsets.all(2),
                        color: Colors.red,
                        child: Text('Perodua Alza', style: TextStyle(fontSize: 10, color: Colors.white, fontWeight: FontWeight.w600),),
                      ),
                      Container(
                        width: 1000,
                        padding: EdgeInsets.all(2),
                        child: Text('100 Rent', style: TextStyle(fontSize: 10, color: Colors.white, fontStyle: FontStyle.italic),),
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius:  BorderRadius.only(bottomRight:  Radius.circular(5),bottomLeft: Radius.circular(5)),
                        ),
                      ),

                    ],
                    ),
                   
                    ),onTap:(){
                    print("you clicked my");
                    }
                  )
                  ],
                  ),
                ),
              ],
              ),
            ) 
       ],
       ),
    );
  }
}

class CurrentBooking extends StatefulWidget {
  CurrentBooking({Key key}) : super(key: key);

  @override
  _CurrentBookingState createState() => _CurrentBookingState();
}

class _CurrentBookingState extends State<CurrentBooking> {
  @override
  Widget build(BuildContext context) {
    return Container(
       margin: EdgeInsets.all(10),
           padding: EdgeInsets.all(5),
           width: 1000,
           height: 120,
           decoration: BoxDecoration(
             color: Colors.redAccent[400],
             borderRadius: BorderRadius.circular(20),
             boxShadow:  [
                        BoxShadow(
                          color: Colors.blueAccent.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 5,
                          // offset: Offset(1, 1), changes position of shadow
                        ),
                        ],
           ),
         child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal:5, vertical:4),
            child: Text("Arif's Booking",style: TextStyle(color: Colors.white, fontSize: 20),textAlign: TextAlign.center,),
          ),
         // Divider(color: Colors.white),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
              Container(
                 padding: EdgeInsets.symmetric(horizontal:5, vertical:4),  
                  child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[
                 Text('From', style: TextStyle(color: Colors.grey[400], fontSize: 10),),
                 Text('Aug, 17 2019 ', style: TextStyle(color: Colors.grey[300],fontSize: 10)),
                 Text('10:20', style: TextStyle(color: Colors.grey[300],fontSize: 35)),
               ],
               ),
              ),
              Icon(Icons.navigate_next, color: Colors.white,),
              Container(
               padding: EdgeInsets.symmetric(horizontal:5, vertical:4),  
                 child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[
                 Text('From', style: TextStyle(color: Colors.grey[400], fontSize: 10),),
                 Text('Aug, 17 2019 ', style: TextStyle(color: Colors.grey[300],fontSize: 10)),
                 Text('12:20', style: TextStyle(color: Colors.grey[300],fontSize: 35)),
               ],
               ),
             ),

           ],
           ),
         ),
         
       ],
       ),
    );
  }
}

class DetailOrder extends StatefulWidget {
  DetailOrder({Key key}) : super(key: key);

  @override
  _DetailOrderState createState() => _DetailOrderState();
}

class _DetailOrderState extends State<DetailOrder> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),      
       decoration: BoxDecoration(
         color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow:  [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 2,
                          // offset: Offset(1, 1), changes position of shadow
                        ),
                        ],
       ),
       child: Column(
         mainAxisAlignment: MainAxisAlignment.spaceAround,
         children: <Widget>[
         Row(children: <Widget>[
          Icon(Icons.info, color: Colors.red,),
          Flexible(
            child: Container(
              padding: EdgeInsets.all(5),
              child: Text('You need to fill in additional booking information at least 3 hour before booking time', style: TextStyle(
                color: Colors.black, fontSize: 13
              ),
              ),
            ),
          ) 
         ],
         ),
         RaisedButton(
           color: Colors.blueAccent,
           shape: RoundedRectangleBorder(
           borderRadius: BorderRadius.circular(20.0),),
           onPressed: (){},
           child: Text('Continue', style: TextStyle(color: Colors.white,),
         )
         )
       ],
       ),
    );
  }
}