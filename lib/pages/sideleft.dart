import 'package:agtranandroid/main.dart';
import 'package:flutter/material.dart';

class NavDrawer extends StatelessWidget {

  // user defined function
showAlertDialog(BuildContext context) {

  // set up the buttons
  Widget cancelButton = FlatButton(
    child: Text("Tidak"),
    onPressed:  () {
     Navigator.of(context).pop();
    },
  );
  Widget continueButton = FlatButton(
    child: Text("Ya"),
    onPressed:  () {},
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    content: Text("Adakah anda ingin Log Keluar?"),
    actions: [
      cancelButton,
      continueButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            margin: EdgeInsets.all(0.0),
            child: Column(
              children: <Widget>[
                FlatButton(
                  child: Text(' '),
                  onPressed: (){},
                )
              ],
            ),
            decoration: BoxDecoration(
                color: Colors.red[300],
               ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: (){
              Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => MyHomePage(),
                          fullscreenDialog: true, 
                          )
                      );
            },
          ),
          ListTile(
            leading: Icon(Icons.book),
            title: Text('Reservation'),
            onTap: (){
              Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => MyHomePage(),
                          fullscreenDialog: true, 
                          )
                      );
            },
          ),
          ListTile(
            leading: Icon(Icons.account_box),
            title: Text('My Account'),
            onTap: (){
              Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => MyHomePage(),
                          fullscreenDialog: true, 
                          )
                      );
            },
          ),
          ListTile(
            leading: Icon(Icons.room_service),
            title: Text('Service'),
            onTap: (){
              Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => MyHomePage(),
                          fullscreenDialog: true, 
                          )
                      );
            },
          ),
          ListTile(
            leading: Icon(Icons.new_releases),
            title: Text('Promotion'),
            onTap: (){
              Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => MyHomePage(),
                          fullscreenDialog: true, 
                          )
                      );
            },
          ),
          ListTile(
            leading: Icon(Icons.info),
            title: Text('About us'),
            onTap: (){
              Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => MyHomePage(),
                          fullscreenDialog: true, 
                          )
                      );
            },
          ),
          ListTile(
            leading: Icon(Icons.chat_bubble_outline),
            title: Text('Contact us'),
            onTap: (){
              Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => MyHomePage(),
                          fullscreenDialog: true, 
                          )
                      );
            },
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            leading: Icon(Icons.close),
            title: Text('Log Keluar'),
            onTap: (){ showAlertDialog(context);},
          ),
          
        ],
      ),
    );
  }
}