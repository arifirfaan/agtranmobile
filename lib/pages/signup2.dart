import 'package:agtranandroid/pages/signup3.dart';
import 'package:flutter/material.dart';

class SignUp2 extends StatefulWidget {
  SignUp2({Key key}) : super(key: key);

  @override
  _SignUp2State createState() => _SignUp2State();
}

class _SignUp2State extends State<SignUp2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         elevation: 0.0,
         backgroundColor: Colors.white,
         leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black,),
          onPressed: (){}
          ),
       ),
       body: Container(
         child: ListView(
           children: <Widget>[
             ListTile(
               title: Text('Sign Up', style: TextStyle(color: Colors.black, fontSize: 30),),
             ),
             ListTile(
               title: Text("Make sure your age at least 19 years old and have a Driver's Licence to register as a AGTRAN.com member.",
                 style: TextStyle(fontSize: 13),
                 ),
             ),
             ListTile(
               title: Container( 
               alignment: Alignment.centerLeft,
               child: Text('E-mail', style: TextStyle(fontSize: 15),),
             ),
             ),
             ListTile(
               title: TextField(
              decoration: InputDecoration(
              hintText: "youremail@email.com",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5)
              ),
             ),
             ),
             ),
             ListTile(
               title: Container(
               padding: EdgeInsets.symmetric(vertical:5), 
               alignment: Alignment.centerLeft,
               child: Text('Password', style: TextStyle(fontSize: 15),),
             ),
             ),
             ListTile(
               title: TextField(
              obscureText: true,   
              decoration: InputDecoration(
              hintText: "create password",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5)
              ),
             ),
             ),
             ),
             ListTile(
               title: Container( 
               alignment: Alignment.centerLeft,
               child: Text('Confirm Password', style: TextStyle(fontSize: 15),),
             ),
             ),
             ListTile(
               title: TextField(
                 obscureText: true,
              decoration: InputDecoration(
              hintText: "re-enter password",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5)
              ),
             ),
             ),
             ),
             ListTile(
               title: SizedBox(
                 child: RaisedButton(
                   color: Colors.lightBlueAccent,
                   onPressed: (){
                     Navigator.push(context,
                     MaterialPageRoute(
                       builder: (_) => SignUp3() 
                     ));
                   },
                   child: Text('Next', style: TextStyle(color: Colors.white),),
                 )
               ),
             )
           ],
         ),
       )
       
    );
  }
}