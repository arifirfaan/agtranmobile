import 'package:agtranandroid/pages/signup.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body: Center(
         child: Container(
         margin: EdgeInsets.all(10),
         child: ListView(children: <Widget>[
          Flexible(
            child:  Container(height: 20,),
          ),
           Align(
             alignment: Alignment.topCenter,
             child: Container(
               child: Text('AGTRAN', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 30),),
             ),
           ),
           Flexible(
             child:  Container(
             height: 20,
           ),
           ),
           Flexible(
             child: TextField(decoration: InputDecoration(
              hintText: "E-mail",
              border: OutlineInputBorder(
                 borderRadius: BorderRadius.circular(10)
              ),
            ),
             ),
           ),
           Container(
             height: 20,
           ),
           Flexible(
             child: TextField(decoration: InputDecoration(
              hintText: "Password",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10)
              ),
            ),
             ),
           ),
           FlatButton(
             onPressed: (){},
             child: Text('Forgot Password?', style: TextStyle(color: Colors.blue, fontSize: 11),),
           ),
           Container(
             height: 10,
           ),
           SizedBox(
             width: 1000,
             height: 50,
             child: RaisedButton(
             color: Colors.lightBlueAccent,
             onPressed: (){},
             child: Text('Login',style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 15),),
           ),
           ),
           Align(
             alignment: Alignment.topCenter,
             child: Container(
               margin: EdgeInsets.all(10),
               child: Text('Or', style: TextStyle(fontSize: 15),),
             ),
           ),
           SizedBox(
             width: 1000,
             height: 50,
             child: RaisedButton(
             color: Colors.blueAccent,
             onPressed: (){},
             child: Text('Continue with Google',style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 15),),
           ),
           ),
           Container(
             height: 10,
           ),
          FlatButton(
             onPressed: (){
               Navigator.push(context, MaterialPageRoute(
                 builder: (_) => SignUp()
               ));
             },
             child: Text('Did not have account? \n Sign up!', style: TextStyle(color: Colors.grey, fontSize: 14),textAlign: TextAlign.center,),
           ),
         ],
         ),
       ),
       )
    );
  }
}