import 'package:flutter/material.dart';

class SignUp3 extends StatefulWidget {
  SignUp3({Key key}) : super(key: key);

  @override
  _SignUp3State createState() => _SignUp3State();
}

class _SignUp3State extends State<SignUp3> {

 String _valSusun;

  List _mySusun = ["+60","+08"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         elevation: 0.0,
         backgroundColor: Colors.white,
         leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black,),
          onPressed: (){}
          ),
          ),
       body: Container(
         margin: EdgeInsets.all(5),
         child: ListView(
          children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
            child: Text('Personal Information'),
          ),
          IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new Flexible(
                child: Container(  
                margin: EdgeInsets.symmetric(horizontal: 10),  
                child: new TextField(
                decoration: InputDecoration(
                  hintText: "First Name",
                  focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent, width: 2.0),
                  ),
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5)
                ),
                ),
                ),
                )
               ),
                new Flexible(
                child: Container(
                margin: EdgeInsets.symmetric(horizontal: 5),  
                child: new TextField(
                decoration: InputDecoration(
                  hintText: "Last Name",
                  focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent, width: 2.0),
                  ),
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5)
                ),
                ),
                ),
                )
               ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
            child: TextField(
                decoration: InputDecoration(
                  hintText: "Date of Birth",
                  focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent, width: 2.0),
                  ),
                  border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),  
                  borderRadius: BorderRadius.circular(5)
                ),
                ),
                ),
          ),
          Container(height: 30,),
          Container(
            margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
            child: Text('IC/Password Number'),
          ),
          Container(
             margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
            child: TextField(
                decoration: InputDecoration(
                  hintText: "Your IC/Passport Number",
                  focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent, width: 2.0),
                  ),
                  border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),  
                  borderRadius: BorderRadius.circular(5)
                ),
                ),
                ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal:10, vertical: 1),
            child: Text("If you're Malaysian, please enter IC Number", style: TextStyle(color: Colors.grey),),
          ),
          Container(height: 30,),
          Container(
            margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
            child: Text("Driver's License expiry date"),
          ),
          Container(
             margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
            child: TextField(
                decoration: InputDecoration(
                  hintText: "Expiry date",
                  focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent, width: 2.0),
                  ),
                  border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),  
                  borderRadius: BorderRadius.circular(5)
                ),
                ),
                ),
          ),
          Container(height: 30,),
          Container(
            margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
            child: Text("Mobile Number Verification"),
          ),
          IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new Flexible(
                child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10),  
                decoration: ShapeDecoration(
                      shape: RoundedRectangleBorder(
                        side: BorderSide(width: 1.0, style: BorderStyle.solid),
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      ),
                    ),  
                width: 100,    
                margin: EdgeInsets.symmetric(horizontal: 10),  
                child: DropdownButtonHideUnderline(
                child: DropdownButton(  
                hint: Text("+60",style: TextStyle(color: Colors.white,fontSize: 13),),
                value: _valSusun,
                items: _mySusun.map((value){
                  return DropdownMenuItem(
                    child: Text(value,style: TextStyle(color: Colors.black,fontSize: 15),textAlign: TextAlign.center,),
                    value: value,
                  );
                }).toList(),
                onChanged: (value){
                  setState(() {
                    _valSusun = value; //untuk ubah isi list Kategori Barang
                  });
                },
              )
                ) 
                )
               ),
                new Flexible(
                child: Container(
                width: 1000,
                margin: EdgeInsets.symmetric(horizontal: 5),  
                child: new TextField(
                decoration: InputDecoration(
                  hintText: "Mobile Number",
                  focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent, width: 2.0),
                  ),
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5)
                ),
                ),
                ),
                )
               ),
              ],
            ),
          ),
          Container(
            width: 1000,
            height: 60,
            margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
            child: RaisedButton(
              onPressed: (){},
              child: Text('Requested for verification code', style: TextStyle(fontWeight: FontWeight.w700),
              ),
            ),
          ),
          IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new Flexible(
                child: Container(     
                margin: EdgeInsets.symmetric(horizontal: 10),  
                child: new TextField(
                decoration: InputDecoration(
                  hintText: "Enter Code here",
                  focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent, width: 2.0),
                  ),
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5)
                ),
                ),
                ),
                )
               ),
                new Flexible(
                child: Container(
                width: 1000,
                margin: EdgeInsets.symmetric(horizontal: 5),  
                child: RaisedButton(
                  onPressed: (){},
                  child: Text('Confirm',style: TextStyle(fontWeight: FontWeight.w700),)
                )
                )
               ),
              ],
            ),
          ),
          Container(
            height: 20,
          )
        ],
         )
       )   
    );
  }
}