import 'package:agtranandroid/pages/signup2.dart';
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  SignUp({Key key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black,),
          onPressed: (){
            Navigator.of(context).pop();
          },
        )
      ),
       body: Center(
         child: Column(children: <Widget>[
           Flexible(
             child: Container(
               margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
               child: Text('Welcome to AGTRAN.com!', style: TextStyle(fontSize: 25, color: Colors.black),),
             ),
           ),
           Flexible(
             child: Container(
               margin: EdgeInsets.symmetric(horizontal: 15,vertical: 10),
               child: Text("Hello there, revervation can be made as fast as your internet could before you'd like to drive"),
             ),
           ),
           Expanded(
              child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: RaisedButton(
                  color: Colors.lightBlueAccent,
                  onPressed: (){
                     Navigator.push(context, MaterialPageRoute(
                 builder: (_) => SignUp2()
               ));
                  },
                  child: Text('Sign up via e-mail account', style: TextStyle(color: Colors.white),),
                )
            ),
  ),
),

         ],
         ),
       ),
    );
  }
}