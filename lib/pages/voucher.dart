
import 'package:agtranandroid/pages/sideleft.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

class Voucher extends StatefulWidget {
  Voucher({Key key}) : super(key: key);

  @override
  _VoucherState createState() => _VoucherState();
}

class _VoucherState extends State<Voucher> {

var _voucher;

  Future<String> _getJson() async {
    return await DefaultAssetBundle.of(context).loadString("assets/alamat.json");
  }

  @override

void initState() {
    // implement initState
    super.initState();

    _getJson().then((onValue) {
      setState(() {
        _voucher = jsonDecode(onValue);
      });
    });
  }  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: NavDrawer(),
       appBar: AppBar(
         elevation: 0.0,
         title:Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
          Text('AGTRAN', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600, color: Colors.white),),
          Text('.com', style: TextStyle(fontSize: 15, color: Colors.white))
        ],
        ),
       ),
       body:ListView(children: <Widget>[
            Container(
              width: 1000,
              color: Colors.red,
              padding: EdgeInsets.symmetric(horizontal:20, vertical:10),
              child: Text('Voucher', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600, color: Colors.white),),
            ),
            Container(
              height: 20,
            ),
            FutureBuilder(
                initialData: null,
                future: _getJson(),
                builder: (context, snapshot) {
                  if (snapshot.data == null) {
                    return Container(
                      height: 300,
                      child: Center(child: CircularProgressIndicator()),
                    );
                  } else {
                    List<dynamic> _voucher = jsonDecode(snapshot.data);
                    return Expanded(
                      child: ListView.builder(
                        physics: ScrollPhysics(),
                        itemCount: _voucher.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey
                              )
                            ),
                            margin: EdgeInsets.symmetric(horizontal:10,vertical:0),
                            child: RaisedButton(
                            onPressed: (){
                              print('alamat');
                            },
                            
                            color: Colors.white,
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            child: Row(
                              children: <Widget>[
                                Flexible(
                                  child: Container(
                                  margin: EdgeInsets.symmetric(horizontal: 1),
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        _voucher[index]['nama'],
                                        style: TextStyle(
                                          fontSize: 13,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(height: 5),
                                      Text(
                                        _voucher[index]['alamat'],
                                        style: TextStyle(fontSize: 11),
                                      ),
                                      
                                    ],
                                  ),
                                ),
                                ),
                                
                              ],
                            ),
                            ),
                          );
                        },
                      ),
                    );
                  }
                })
       ],
       ),
    );
  }
}