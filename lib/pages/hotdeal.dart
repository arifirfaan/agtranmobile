import 'package:agtranandroid/pages/senaraihotdeal.dart';
import 'package:agtranandroid/pages/sideleft.dart';
import 'package:flutter/material.dart';

class HotDeal extends StatefulWidget {
  HotDeal({Key key}) : super(key: key);

  @override
  _HotDealState createState() => _HotDealState();
}

class _HotDealState extends State<HotDeal> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: NavDrawer(),
       appBar: AppBar(
         elevation: 0.0,
         title:Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
          Text('AGTRAN', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600, color: Colors.white),),
          Text('.com', style: TextStyle(fontSize: 15, color: Colors.white))
        ],
        ),
       ),
       body:ListView(children: <Widget>[
            Container(
              width: 1000,
              color: Colors.red,
              padding: EdgeInsets.symmetric(horizontal:20, vertical:10),
              child: Text('Hot Deal', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600, color: Colors.white),),
            ),
            Container(
              height: 20,
            ),
            Container(
              child: SenaraiHotDeal(),
            )
       ],
       ),
    );
  }
}