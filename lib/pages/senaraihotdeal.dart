import 'package:flutter/material.dart';
import 'dart:convert';

class SenaraiHotDeal extends StatefulWidget {
  SenaraiHotDeal({Key key}) : super(key: key);

  @override
  _SenaraiHotDealState createState() => _SenaraiHotDealState();
}

class _SenaraiHotDealState extends State<SenaraiHotDeal> {

var _hotdeal;

  Future<String> _getJson() async {
    return await DefaultAssetBundle.of(context).loadString("assets/hotdeal.json");
  }

  @override

void initState() {
    // implement initState
    super.initState();

    _getJson().then((onValue) {
      setState(() {
        _hotdeal = jsonDecode(onValue);
      });
    });
  }  

  @override
  Widget build(BuildContext context) {
    return Container(
       child: FutureBuilder(
                initialData: null,
                future: _getJson(),
                builder: (context, snapshot) {
                  if (snapshot.data == null) {
                    return Container(
                      height: 300,
                      child: Center(child: CircularProgressIndicator()),
                    );
                  } else {
                    List<dynamic> _hotdeal = jsonDecode(snapshot.data);
                    return Expanded(
                      child: ListView.builder(
                        physics: ScrollPhysics(),
                        itemCount: _hotdeal.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            width: 100,
                            height: 100,
                            decoration: BoxDecoration(
                              color: Colors.red,
                            ),
                            child: Text(
                              _hotdeal[index]['model']
                              )
                          );
                        },
                      ),
                    );
                  }
                }),
    );
  }
}