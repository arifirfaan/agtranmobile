import 'package:agtranandroid/pages/hotdeal.dart';
import 'package:agtranandroid/pages/topcar.dart';
import 'package:agtranandroid/pages/voucher.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:agtranandroid/pages/homepages.dart';
import 'package:agtranandroid/pages/login.dart';
import 'package:agtranandroid/pages/sideleft.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

 

  @override
State<StatefulWidget> createState() {
    return _MyHomePageState();
  }
  } 

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {
  
List<dynamic> _tabList = [Tab1(), Voucher(),Voucher(), Tab1(),Voucher(),];
  List<Widget> _iconList = [
    Icon(
      Icons.home,
      size: 20,
      color: Colors.white,
    ),
    Icon(
      Icons.room_service,
      size: 20,
      color: Colors.white,
    ),
    Icon(
      Icons.apps,
      size: 20,
      color: Colors.white,
    ),
    Icon(
      Icons.settings,
      size: 20,
      color: Colors.white,
    ),
    Icon(
      Icons.person,
      size: 20,
      color: Colors.white,
    ),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();

    _tabController = TabController(
      initialIndex: 0,
      length: _tabList.length,
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      drawer: NavDrawer(),
      bottomNavigationBar: CurvedNavigationBar(
        color: Colors.blueAccent,
        height: 50,
        backgroundColor: Colors.white,
        animationCurve: Curves.elasticOut,
        buttonBackgroundColor: Colors.blueAccent,
        items: _iconList,
        onTap: (index) {
          setState(() {
            _tabController.animateTo(index);
          });
        },
      ),
      appBar: AppBar(
        iconTheme: new IconThemeData(color: Colors.white),
        elevation: 0.0,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
          Text('AGTRAN', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600, color: Colors.white),),
          Text('.com', style: TextStyle(fontSize: 15, color: Colors.white))
        ],
        ),
        
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.person,color: Colors.white,),
             onPressed: (){
               Navigator.push(context,
               MaterialPageRoute(
                 builder: (_) => Login()
               ));
             }
          )
        ],
      ),
      body: 
        Stack(
        children: <Widget>[

                        Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                               colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
                                image: AssetImage('assets/images/homewall2.png'),
                                fit: BoxFit.fitHeight),
                          ),
                        ),
                        Container(
                          color: Color.fromRGBO(255, 255, 255, 0.19),
                        ),
                        Container(
                          child: Homepages(), 
                        ),
                        
        ]
      ),  // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
      
      
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
